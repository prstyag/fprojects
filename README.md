<h1 align="center">FINAL PROJECT</h1>

![Watch the video demo](/public/images/fproject/demo.mp4 "Watch the video demo")

## Kelompok 25

- Iis aoliya (@iisaoliya)
- Riska putri g (@Riskapg)
- Agung Prasetyo (@prstyag)

## Tema

Website Penerbitan Berita

## Links Video Demo

- **[Gitlab](https://gitlab.com/prstyag/fprojects/-/blob/master/README.md)**
- Iis aoliya (@iisaoliya) **[Google Drive](https://drive.google.com/drive/folders/1oThk5S6J17SeDaB6oztSN_F-E7CVbetX?usp=sharing)**
- Riska putri g (@Riskapg) **[Google Drive](https://drive.google.com/drive/folders/1fYeEXDCXyR1d9OaYKmuoehsk062QM57v?usp=sharing)**
- Agung Prasetyo (@prstyag) **[Google Drive](https://drive.google.com/file/d/1OKNFysxxZxPqpBBGiyH3t7giwIrFQ3xN/view?usp=sharing)**

## Links Deploy

[Level News](http://levelnews.herokuapp.com/).

## ERD

![ERD levelnews](/public/images/fproject/erd.png "ERD levelnews")

## Seeder

**Default User and Role** 

- **Admin** => Email: `admin@levelnews.com`, Password: `password`
- **Blogwritter** => Email: `blogwritter@levelnews.com`, Password: `password`
- **User** => Email: `user@levelnews.com`, Password: `password`

**User Admin for Check Profile**

- **Agung Prasetyo** => Email: `agung@gmail.com`, Password: `agung12345`

## Alur Aplikasi

- **Login** or **Register**
- Jika **Register** user anda akan diarahkan ke home user. 
- Jika **Login** `Admin` atau `Blogwitter` anda akan diarahkan ke `dashboard Admin`. 
- Didalam dashboard admin terdapat 2 permission yaitu admin dan blogwritter, keduanya memiliki dashboard yang berbeda.

![Dashboard Admin](/public/images/fproject/dashboardAdmin.png "Dashboard Admin")

`gambar diatas` Berikut adalah dashboard admin, yang memiliki hak akses paling tertinggi, mulai dari `Users`, `Permissions`, `Roles`, hinggga `Trash Bin post`.

![Dashboard Blogwritter](/public/images/fproject/dashboardBlogwriter.png "Dashboard Blogwritter")

`gambar diatas` Berikut adalah dashboard blogwritter, yang memiliki hak akses hanya dari `Post`, `Category`, dan `Tag`. Hal tersebut dibuat agar blogwritter dapat fokus untuk membuat `Berita` saja.

![Home All users](/public/images/fproject/home.png "Home All users")

`gambar diatas` Berikut adalah `Home all users`, yang berfokus pada penerbitan berita.

- Details selanjutnya ada di `Video demo` yah!

- Terimakasih!

## UI/UX Template 

Bootstrap, CSS, Photoshop

## Library/Package

- laravel/helpers
- realrashid/sweet-alert
- santigarcor/laratrust
- laravelcollective/html

## Sumber Ref Template

- Frontend [colorlib](https://colorlib.com/).
- Backend [admin stisla](https://getstisla.com/).
- Login or Register, and etc. [google](https://google.com/).

