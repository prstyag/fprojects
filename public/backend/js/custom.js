/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";

//  use previewFile Photo Upload function 

function previewFile(input){
    var file=$("input[type=file]").get(0).files[0];

    if(file)
    {
        var reader = new FileReader();
        reader.onload = function(){
            $('#previewImg').attr("src", reader.result);
        }
        reader.readAsDataURL(file);
    }
}

//  using Jquery smooth fading Alert function 

$(".alert").fadeTo(4000, 1000).slideUp(1000, function(){
    $(".alert").slideUp(1000);
});

//  use Time preloader function 

$(function(){
    setTimeout(()=> {
        $(".loader").fadeOut(500);
    }, 1350);
});