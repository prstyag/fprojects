<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'category_id'=> random_int(1,8),
        'title' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'slug' => Str::slug($faker->sentence($nbWords = 10, $variableNbWords = true)),
        'gambar' => '1615597525.close-up.jpg',
        'content' => $faker->paragraph($nbSentences = 20, $variableNbSentences = true),
    ];
});
