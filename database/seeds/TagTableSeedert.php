<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'name' => 'Terkini',
            'slug' => 'terkini',
        ]);
        Tag::create([
            'name' => 'Hot',
            'slug' => 'hot',
        ]);
        Tag::create([
            'name' => 'Terbaru',
            'slug' => 'terbaru',
        ]);
    }
}
