<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'HTML',
            'slug' => 'html',
        ]);
        Category::create([
            'name' => 'CSS',
            'slug' => 'css',
        ]);
        Category::create([
            'name' => 'Javascript',
            'slug' => 'javascript',
        ]);
        Category::create([
            'name' => 'GIS and Remote Sensing',
            'slug' => 'gis',
        ]);
        Category::create([
            'name' => 'Data Base',
            'slug' => 'mysql',
        ]);
        Category::create([
            'name' => 'Arduino',
            'slug' => 'arduino',
        ]);
        Category::create([
            'name' => 'Web Mapping',
            'slug' => 'webmapping',
        ]);
        Category::create([
            'name' => 'Web Designe',
            'slug' => 'webdesigne',
        ]);
    }
}
