<?php

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});

// Users & All Roles Control Panel Routes
Route::get('/home', [App\Http\Controllers\HomeController::class, 'indexUser'])->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'middleware' => ['role:blogwritter|admin']], function () {
    Route::get('/dashboard', [AdminController::class, 'index'])->name('admin');
    Route::get('/profile', [AdminController::class, 'showProfile'])->name('profile');
    Route::get('/profile/{id}/edit', [AdminController::class, 'editProfile'])->name('edit.profile');
    Route::put('/profile/{id}/update', [AdminController::class, 'updateProfile'])->name('update.profile');

    //  Roles Control Admin, Blogwriter

    //  SoftDelete
    Route::get('/post/trashed', [App\Http\Controllers\PostController::class, 'trashed'])->name('trashed');
    Route::get('/post/restore/{id}', [App\Http\Controllers\PostController::class, 'restore'])->name('restored');
    Route::delete('/post/delete/{id}', [App\Http\Controllers\PostController::class, 'kill'])->name('forcedelete');

    //  Coments Post
    Route::post('/coments/{post}', 'CommentController@store')->name('comment.store');

    Route::resource('post', PostController::class);
    Route::resource('category', CategoryController::class);
    Route::resource('tag', TagController::class);
    
    //  Roles Control Admin,
    Route::resource('users', UsersController::class);
    Route::resource('permission', PermissionController::class);
    Route::resource('roles', RolesController::class);
});

// test iis aoliya//
