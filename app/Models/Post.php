<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // use HasFactory;
    use SoftDeletes;

    protected $fillable = ['title', 'category_id', 'user_id', 'content', 'gambar', 'slug'];
    public $timestamps = true;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function user() {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag');
    }

    public function comments() {
        return $this->belongsToMany('App\Models\Comments');
    }
}
