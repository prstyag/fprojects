<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(8);
        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::paginate(8);
        return view('admin.post.create', compact('posts', 'tags', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'category_id' => 'required',
            'content' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $gambar = $request->file('gambar');
        $image_name = time().'.'.$gambar->getClientOriginalName();
        // $gambar->storeAs('images', $image_name, 'public');
        $gambar->move(public_path('images'),$image_name);


        $posts = new Post();
        $posts->title = $request->input('title');
        $posts->slug = str_slug( $request->input('title'));
        $posts->category_id = $request->input('category_id');
        $posts->content = $request->input('content');
        $posts->gambar = $image_name;
        $posts->user_id = Auth::user()->id;
        $posts->save();

        $posts->tags()->attach($request->tags);
        return redirect()->back()->with('success', 'New Post was successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::where('slug', $slug)->first();
        return view('admin.post.show', compact('posts', 'tags', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::where('slug', $slug)->first();
        return view('admin.post.edit', compact('posts', 'tags', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'title' => 'required',
            'category_id' => 'required',
            'content' => 'required',
        ]);

        $posts = Post::where('slug', $slug)->first();
        if($request->hasFile('gambar')){
            $gambar = $request->file('gambar');
            $image_name = time().'.'.$gambar->getClientOriginalName();
            $gambar->move(public_path('images'), $image_name);

            // Storage::delete('/public/images'. $posts->gambar);

            $posts->title = $request->input('title');
            $posts->category_id = $request->input('category_id');
            $posts->content = $request->input('content');
            $posts->slug = str_slug($request->input('title'));
            $posts->gambar =$image_name;
        } 
        else {
            $posts->title = $request->input('title');
            $posts->category_id = $request->input('category_id');
            $posts->content = $request->input('content');
            $posts->slug = str_slug($request->input('title'));
        }

        $posts->tags()->sync($request->tags);
        $posts->update();
        
        return redirect('/admin/post')->with('success', 'Post successfully update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $posts = Post::where('slug', $slug)->first();
        $posts->delete();

        return redirect()->back()->with('success', 'Post successfully delete!');
    }

    public function trashed(){
        $posts = Post::onlyTrashed()->paginate(8);
        return view('admin.post.trashed', compact('posts'));
    }

    public function restore($id){
        $posts = Post::withTrashed()->where('id', $id)->first();
        $posts->restore();

        return redirect()->back()->with('success', 'Post was successfully restored!');
    }

    public function kill($id){
        $posts = Post::withTrashed()->where('id', $id)->first();
        $posts->forceDelete();

        return redirect()->back()->with('success', 'Post was successfully delete!');
    }
}