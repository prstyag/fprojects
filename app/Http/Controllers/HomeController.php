<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $posts = Post::latest()->take(6)->get();
        return view('layouts.frontend'); //, compact('posts')
    }

    public function indexUser()
    {
        return view('home');
    }
}
