<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function __construct()
    {
        // $this->middleware('role:owner|admin|blogwriter');
            $this->middleware('auth');
        
    }

    public function index()
    {
        $data = [];
        $n_users = User::all()->count();

        // Count all Roles Blogwritter
        $n_blogwr = User::with('roles')->whereHas('roles', function($query) {
            $query->where('name', 'blogwritter');
            })->count();

        // Count all Roles Administrator
        $n_admin = User::with('roles')->whereHas('roles', function($query) {
            $query->where('name', 'admin');
            })->count();
        $n_post = Post::all()->count();
        $data = [
            'n_users' => $n_users,
            'n_post' => $n_post,
            'n_blogwr' => $n_blogwr,
            'n_admin' => $n_admin,

        ];
        return view('admin.dashboard', $data);
    }

    public function showProfile()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.profile.view', compact('user'));
    }

    public function editProfile()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.profile.edit', compact('user'));
    }

    public function updateProfile(Request $request, $id)
    {
        // try {
            $this->validate($request, [
                'name' => 'required',
                'username' => ['required', 'string', 'min:2', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'regex:/^((62)|(08))[0-9]/', 'not_regex:/[a-z]/', 'min:11', 'max:13'],
                'email' => 'required|email|unique:users,email,' . $id,
                'photo' => 'sometimes|image|mimes:jpg,png,jpeg|max:9000',
            ]);

            $user = User::findOrFail(Auth::user()->id);
            // if ($request->photo != null){

            //     // Avatar
            //     $photo = $request->photo;
            //     $photoName = Str::slug($request->name, '-') . uniqid() . '.' . $photo->getClientOriginalExtension();
                
            //     if(!Storage::disk('public')->exists('user')){
            //         Storage::disk('public')->makeDirectory('user');
            //     }

            //     // delete old images
            //     if(Storage::disk('public')->exists('user/' . $user->photo)){
            //         Storage::disk('public')->delete('user/' . $user->photo);
            //     }
            //     //Store 
            //     //avatar $image->storeAs('user', '$photoName', 'public')
            //     $userImg = Image::make($photo)->fit(200,200)->stream();
            //     // ->resize(640, null, function ($constraint) {
            //     //     $constraint->aspectRatio();
            //     //     $constraint->upsize();
            //     // })->stream();
            //     Storage::disk('public')->put('user' . $photoName, $userImg);
            // }else{
            //     $photoName = $user->photo;
            // }
            if($request->hasFile('photo')){
                $photo = $request->file('photo');
                $photo_name = time().'.'.$photo->getClientOriginalName();
                $photo->move(public_path('images/profile/'), $photo_name);

                // Storage::delete('/public/images'. $posts->gambar);

                $user->name = $request->input('name');
                $user->username = $request->input('username');
                $user->address = $request->input('address');
                $user->phone = $request->input('phone');
                $user->email = $request->input('email');
                $user->photo =$photo;
            } 
            else {
                $user->name = $request->input('name');
                $user->username = $request->input('username');
                $user->address = $request->input('address');
                $user->phone = $request->input('phone');
                $user->email = $request->input('email');
            }

            // $user->name = $request->input('name');
            // $user->username = $request->input('username');
            // $user->address = $request->input('address');
            // $user->phone = $request->input('phone');
            // $user->photo = $photoName;
            // $user->email = $request->input('email');
            $user->update();

            return redirect()->route('profile')->with('success', "Profile $user->name has successfully been updated.");
        // } catch (ModelNotFoundException $ex) {
        //     if ($ex instanceof ModelNotFoundException) {
        //         return response()->view('errors.' . '404');
        //     }
        // }
    }

}
