@extends('layouts.admin')
@section('sub-title', 'Users')
@section('location', 'Users')
@section('content')
  
      <div class="clearfix mt-4">
        <a href="{{route('users.create')}}" class="btn btn-info mb-2">Add User</a>
      </div>
      <div class="card">
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                {{-- <th scope="col">Permissions</th> --}}
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                      @foreach($user->roles as $r)
                          {{$r->display_name}}
                      @endforeach
                      </td>
                      {{-- <td> --}}
                        {{-- {{$n_perms}} --}}
                        {{-- @foreach($user->permissions as $p)
                          {{$p->Count}}
                        @endforeach --}}
                      {{-- </td> --}}
                      <td>
                          {{-- <form action="{{ route('users.show', $user->id) }}" method="POST">
                              @csrf
                              @method('delete')
                              <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                              <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                            </form> --}}
                        <div>
                            <a class="btn btn-primary" href="{{ route('users.edit', $user->id) }}" class="btn btn-info" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i> </a>
                            <a class="btn btn-danger" href="{{ route('users.show', $user->id) }}" class="btn btn-danger" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i> </a>
                        </div>
                      </td>
                    </tr>
                    @endforeach
            </tbody>
          </table>
          <!-- Pager -->
          <div class="clearfix mt-4">
            {{$users->links('vendor.pagination.bootstrap-4')}}
          </div>
        </div>
      </div>
@endsection