<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="{{route('home')}}">{{ config('app.name') }}</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{route('home')}}">LN</a>
      </div>
      <ul class="sidebar-menu">
          <li class="menu-header">Dashboard Menu</li>
          <li class="nav-item dropdown">
          <a href="{{route('admin')}}" class="nav-link {{ Request::is('dashboard') ? ' active' : null }}"><i class="fas fa-fire"></i><span>Dashboard</span></a>
          </li>
          {{-- <li class="menu-header">Starter</li> --}}
          <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-newspaper"></i> <span>Post</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link" href="{{route('post.index')}}"><i class="fas fa-list-ul" style="margin: 0"></i>List Post</a></li>
              <li><a class="nav-link beep beep-sidebar" href="{{route('post.create')}}"><i class="far fa-plus-square" style="margin: 0"></i>Add Post</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-stream"></i> <span>Kategory</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{route('category.index')}}"><i class="fas fa-list-ul" style="margin: 0"></i>List Kategory</a></li>
              <li><a class="nav-link beep beep-sidebar" href="{{route('category.create')}}"><i class="far fa-plus-square" style="margin: 0"></i>Add Kategory</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fab fa-slack-hash"></i> <span>Tag</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{route('tag.index')}}"><i class="fas fa-list-ul" style="margin: 0"></i>List Tags</a></li>
              <li><a class="nav-link beep beep-sidebar" href="{{route('tag.create')}}"><i class="far fa-plus-square" style="margin: 0"></i>Add Tags</a></li>
            </ul>
          </li>
          @role(['admin'])
            <li class="menu-header">SDM & Permissions</li>
            <li><a class="nav-link" href="{{route('users.index')}}"><i class="fas fa-user"></i> <span>Users</span></a></li>
            <li><a class="nav-link" href="{{route('roles.index')}}"><i class="fas fa-eye-slash"></i> <span>Roles</span></a></li>
            <li><a class="nav-link" href="{{route('permission.index')}}"><i class="fas fa-info"></i> <span>Permissions</span></a></li>
            <li><a class="nav-link" href="{{route('trashed')}}"><i class="far fa-trash-alt"></i> <span>Trash Bin</span></a></li>
          @endrole
        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
          <a href="{{ url('/') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
            <i class="fas fa-rocket"></i> {{ config('app.name') }}
          </a>
        </div> 
    </aside>
  </div>