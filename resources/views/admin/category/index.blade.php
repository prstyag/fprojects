@extends('layouts.admin')
@section('sub-title', 'Category')
@section('location', 'Category')
@section('content')

    <div class="clearfix mt-4">
    <a href="{{route('category.create')}}" class="btn btn-info mb-2">Add Kategory</a>
    </div>
    
    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Name Category</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    @if (count($categories) > 0)
                    <tbody>
                        <?php $no = 0;?>
                        @foreach ($categories as $category)
                        <?php $no++ ;?>
                        <tr>
                        {{-- <th scope="row">{{ $category + $categories->first()->$categori }}</th> --}}
                        <th scope="row">{{ $category->id }}</th>
                        <td>{{ $category->created_at }}</td>
                        <td>{{ $category->name }}</td>
                        <td>
                            <form action="{{ route('category.destroy', $category->id) }}" method="POST">
                                @csrf
                                @method('delete')
                                <a href="{{ route('category.edit', $category->id) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                                <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                        {{$categories->links('vendor.pagination.bootstrap-4')}}
                    @else
                        <td colspan="4" class="text-center" style="color: #6777ef;">                           
                            <b>Category not found. <a href="{{route('category.create')}}" style="font-weight: bold;">Create here!</a></b>
                        </td>
                    @endif
            </table>
        </div>
    </div>
@endsection