@extends('layouts.admin')
@section('sub-title', 'Post')
@section('location', 'Details Post')
@section('content') 
    <div class="clearfix mt-4">
        <a href="{{route('post.index')}}" class="btn btn-info mb-2">Back</a>
    </div>

    <div class="section-body">
        {{-- <h2 class="section-title">Details Post</h2> --}}

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              {{-- <div class="card-header">
                <h4>Details Post</h4>
              </div> --}}
              <div class="card-body">
                <div class="tickets">
                    <div class="ticket-content">
                        <div class="ticket-header">
                            {{-- <div class="ticket-sender-picture img-shadow">
                              {{-- <img src="../assets/img/avatar/avatar-5.png" alt="image">
                            </div> --}}
                            <div class="ticket-detail">
                              <div class="ticket-title">
                                <h4>{{$posts->title}}</h4>
                              </div>
                              <div class="ticket-info">
                                  <p>{{ Auth::user()->name }} <i class="bullet"></i> <span class="text-primary font-weight-600"> 2 min ago</span>
                                      {{-- <div class="font-weight-600">Farhan A. Mujib</div> --}}
                                      {{-- <div class="bullet"></div> --}}
                                      {{-- <div class="text-primary font-weight-600">2 min ago</div> --}}
                                  </p>
                              </div>
                            </div>
                          </div>
                        <div class="ticket-description">
                            <p>{{$posts->content}}</p>

                            <div class="ticket-divider"></div> 
                            
                            <hr>
                            <div class="row">
                                <div class="col-md-6 gallerry">
                                    <p>Tags: 
                                        @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}"
                                @foreach ($posts->tags as $item)
                                    @if ($tag->id == $item->id)
                                        selected
                                    @endif
                                @endforeach
                                >{{ $tag->name }}</option>
                                @endforeach
                                    </p>
                                </div>
                                <div class="col-md-6 gallerry">
                                    <p>Tags2: </p>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
@endsection
