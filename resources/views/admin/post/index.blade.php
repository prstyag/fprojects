@extends('layouts.admin')
@section('sub-title', 'Post')
@section('location', 'Post')
@section('content') 
    <div class="clearfix mt-4">
        <a href="{{route('post.create')}}" class="btn btn-info mb-2">Create Post</a>
        
    </div>
    
    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Name Post</th>
                        <th scope="col">Kategory</th>
                        <th scope="col">Cover</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    @if (count($posts) > 0)
                    @foreach ($posts as $key => $post)
                    <tbody>
                    <tr>
                        <td>{{ $key+1 }}.</td>
                        <td>{{ $post->created_at }}</td>
                        <td>
                            {{ $post->title }}
                            <div class="table-links">
                                in <a href="#">Web Development</a>
                                <div class="bullet"></div>
                                <a href="{{route('post.show', $post->slug)}}">View</a>
                            </div>
                        </td>
                        <td>{{ $post->category->name }}</td>
                    <td><img src="{{ asset('images/'.$post->gambar) }}" alt="{{ $post->judul }}" style="max-width: 50px;margin-top: 2px;"></td>
                        <td>
                        <form action="{{route('post.destroy', $post->slug) }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="{{route('post.edit', $post->slug) }}" class="btn btn-primary btn-action " data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                            <button type="submit" class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                        </td>
                    </tr>
                    </tbody>
                    @endforeach
                    @else
                        <td colspan="6" class="text-center" style="color: #6777ef;">                           
                            <b>Post not found. <a href="{{route('post.create')}}" style="font-weight: bold;">Create here!</a></b>
                        </td>
                    @endif
            </table>
        </div>
    </div>
    <!-- Pager -->
    <div class="clearfix mt-4">
        {{$posts->links('vendor.pagination.bootstrap-4')}}
    </div>
@endsection